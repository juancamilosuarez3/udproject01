from django.shortcuts import render
from django.views.generic.edit import FormView
from .forms import NewDeparatmentoForm
from applications.persona.models import Empleado
from .models import Departamentos


# Create your views here.

class NewDepartamentoView(FormView):
    template_name = 'departamento/new_departamento.html'
    form_class = NewDeparatmentoForm
    success_url = '/'

    def form_valid(self, form):
        print('**** estamos en el form valid***')
        depa = Departamentos(
            name=form.cleaned_data['departamentos'],
            shor_name=form.cleaned_data['shortname']
        )
        depa.save()
        nombre = form.cleaned_data['nombre']
        apellido = form.cleaned_data['apellido']
        Empleado.objects.create(first_name=nombre,
                                last_name=apellido,
                                # el departamento es predetermmiandos
                                job='1',
                                departamentos=depa
                                )

        return super(NewDepartamentoView, self).form_valid(form)
