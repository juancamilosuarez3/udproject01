from django.contrib import admin
from django.urls import path
from applications.departamentos import views


urlpatterns = [
    path('new-departamento/', views.NewDepartamentoView.as_view(), name='nuevo departamento'),
]