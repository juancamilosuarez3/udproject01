from django import forms

class NewDeparatmentoForm(forms.Form):
    nombre = forms.CharField(max_length=60)
    apellido = forms.CharField(max_length=60)
    departamentos = forms.CharField(max_length=60)
    shortname = forms.CharField(max_length=30)
