from django.shortcuts import render
from django.views.generic import TemplateView, ListView, CreateView
from .models import Prueba
from .forms import PruebaForm


# Create your views here.


class PruebaView(TemplateView):
    template_name = 'home/pruebas.html'

class ResumenFoundationView(TemplateView):
    template_name = 'home/resumen_foundation.html'


class PruebaListView(ListView):
    template_name = "home/lista.html"
    context_object_name = 'pepa'
    queryset = ['1', '10', '20', '30']


class ListarPrueba(ListView):
    # siempre retorna un query
    template_name = "home/lista_prueba.html"
    model = Prueba
    context_object_name = 'lista'


class PruebaCreateView(CreateView):
    template_name = "home/add.html"
    model = Prueba
    #primera forma sin formulrio
    #fields = ['titulo', 'subtitulo', 'cantidad']
    #segunda con formulario
    form_class = PruebaForm
    success_url = '/'
