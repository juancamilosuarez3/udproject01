from django import forms

from .models import Prueba

#formularios vinvulados a un modelo
class PruebaForm(forms.ModelForm):
    class Meta:
        model = Prueba
        #los parametros que quiero encontrar en el formulario
        fields = ('titulo', 'subtitulo', 'cantidad',)
        #widgets para personalizar el formulario
        widgets = {
            'titulo': forms.TextInput(
                attrs={
                    'placeholder': 'ingrese texto aqui'})}

    # validacion del de un parametro
    def clean_cantidad(self):
        cantidad = self.cleaned_data.get('cantidad')
        # para validar que la cantidad de libros tiene que ser mayor a 10
        if cantidad < 10:
            # raise es lo que va mustrar en pantalla del por que no es valido
            raise forms.ValidationError('ingrese un numero mayor a 10')
        return cantidad
