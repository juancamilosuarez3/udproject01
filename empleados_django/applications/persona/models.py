from django.db import models
from applications.departamentos.models import Departamentos
from ckeditor.fields import RichTextField

# Create your models here.
class Habilidades(models.Model):
    habilidad = models.CharField('habilidad', max_length=50)

    class Meta:
        verbose_name = 'Habilidad'
        verbose_name_plural = 'Habilidades Empleado'

    def __str__(self):
        return str(self.id) + '-' +self.habilidad


class Empleado(models.Model):
    """Modelo para tabla empleado"""
    JOB_CHOICES = (
        ('O', 'CONTADOR'),
        ('1', 'ADMINISTRADOR'),
        ('2', 'ECONOMISTA'),
        ('3', 'OTRO'),

    )
    first_name = models.CharField('Nombres', max_length=70)
    last_name = models.CharField('Apellido', max_length=70)
    full_name = models.CharField('Nombres completos', max_length=120,
                                 blank=True,)
    job = models.CharField('trabajo', max_length=1, choices=JOB_CHOICES)
    departamentos = models.ForeignKey(Departamentos, on_delete=models.CASCADE)
    #image = models.ImageField(, upload_to=None, height_field='', width_field='',)
    habilidad = models.ManyToManyField(Habilidades)
    #con ayuda del ckeditor podemos agregar un espacio para texto con varias Herramientas como tipo de texto
    hoja_vida = RichTextField()
    class Meta:
        verbose_name = 'Empleado'
        ordering = ['-first_name']


    def __str__(self):
        return str(self.id) + '-' + self.first_name + '-' + self.last_name + '-' + self.job#
