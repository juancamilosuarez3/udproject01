from django.shortcuts import render
from django.views.generic import (ListView, DetailView, CreateView, TemplateView, UpdateView)
from django.urls import reverse_lazy
from .models import Empleado

# Create your views here.
# requerimientos
# lista de todos los empleados de la empresado
from .. import departamentos

class InicioView(TemplateView):
    """vista que carga la pagina de incio """
    template_name = 'inicio.html'

# 1. lista de todos los empleados
class ListAllEmpleados(ListView):
    template_name = 'persona/list_all.html'
    paginate_by = 4
    ordering = ('first_name',)
    model = Empleado


# 2. lista por area de empelados
class ListByAreaEmpleados(ListView):
    template_name = 'persona/list_by_area.html'

    # los dos query hacen lo mismo solo uno esta definido y el otro es commo funcion y fue sacado de la pg ccbv
    #   queryset = Empleado.objects.filter(departamentos__name='otro')
    # con el metodo podemos recoger informacion de la url ccon el para indicar un parametro
    def get_queryset(self):
        # el codigo que yo quiera
        area = self.kwargs['shorname']
        lista = Empleado.objects.filter(departamentos__shor_name=area)
        return lista


# 3. lista de empleados por trabajo requerido en el url
class ListByJobEmpleados(ListView):
    template_name = 'persona/list_by_job.html'

    def get_queryset(self):
        # el codigo que yo quiera
        job = self.kwargs['job']
        lista = Empleado.objects.filter(job=job)
        return lista


# 4.lista de empleados por palabra claves en caja de html
class ListEmpleadosByKword(ListView):
    template_name = 'persona/by_kword.html'
    context_object_name = 'empleados'

    def get_queryset(self):
        print('***********************')
        # asi se recoje y elemento de una caja de texto
        palabra_clave = self.request.GET.get('kword', )
        lista = Empleado.objects.filter(first_name=palabra_clave)
        print('****************', lista)
        return lista


# 5.lista de habilidades de un empleado.
class ListHabilidadesEmpleado(ListView):
    template_name = 'persona/habilidades.html'
    context_object_name = 'habilidades'

    def get_queryset(self):
        palabra_clave = self.request.GET.get('id', )
        empleado = Empleado.objects.get(id=palabra_clave)
        print(empleado.habilidad.all())
        return empleado.habilidad.all()


# detailview
# para ver todos los detalles de un solo empleado por pk

class EmpleadoDetailView(DetailView):
    model = Empleado
    template_name = "persona/detail_empleado.html"

    def get_context_data(self, **kwargs):
        context = super(EmpleadoDetailView, self).get_context_data(**kwargs)
        context['titulo'] = 'empleado del mes'
        return context


class Successview(TemplateView):
    template_name = "persona/success.html"


# vista para agregar empleadosDjango
class EmpleadoCreateView(CreateView):
    template_name = "persona/add.html"
    model = Empleado
    # forma 1 para hacer llama los parametros del modelo a la vista
    fields = ['first_name', 'last_name', 'job', 'departamentos', 'habilidad']
    # forma2 para llmaar todos los parametros del modelo
    # fields = ('__all__')
    # forma 1: el siguiente parametro es para rediccionar despues de completar el form
    # success_url = '/success'
    # forma2 de redireccionar
    success_url = reverse_lazy('persona_app:correct')
#revisa si el form es valido y hace una accion
    def form_valid(self, form):
        # logica del codigo
        #guardar un formulario
        empleado = form.save()
        #print(empleado)
        #ACTUALIZAR formulario (un atributo apartir de otros atributos)
        empleado.full_name = empleado.first_name + ' ' + empleado.last_name
        empleado.save()
        return super(EmpleadoCreateView, self).form_valid(form)

#vista para editar el regustro del modelov

class EmpleadoUpdateView(UpdateView):
    model = Empleado
    template_name = "persona/pdate.html"
    fields = ['first_name', 'last_name', 'job', 'departamentos', 'habilidad']
