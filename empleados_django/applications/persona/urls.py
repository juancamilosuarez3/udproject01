from django.contrib import admin
from django.urls import path
from . import views
from .views import ListAllEmpleados, ListByAreaEmpleados, ListByJobEmpleados, ListEmpleadosByKword, \
    ListHabilidadesEmpleado, EmpleadoDetailView, EmpleadoCreateView, Successview, EmpleadoUpdateView, InicioView

app_name = 'persona_app'

urlpatterns = [
    path('', InicioView.as_view(), name='inicio'),
    path(
        'listar-todo-empleados/',
        ListAllEmpleados.as_view(),
        name = "empleados_all",
    ),
    path('listar-by-area/<shorname>', ListByAreaEmpleados.as_view()),
    path('listar-by-trabajo/<job>', ListByJobEmpleados.as_view()),
    path('buscar-empleado/', ListEmpleadosByKword.as_view()),
    path('listar-habilidades-empleado/', ListHabilidadesEmpleado.as_view()),
    path('ver-empleado/<pk>/', EmpleadoDetailView.as_view()),
    path('add-empleado/', EmpleadoCreateView.as_view()),
    path('success/', Successview.as_view(), name='correct'),
    path('update-empleado/<pk>/', EmpleadoUpdateView.as_view(), name='modificar_empleado'),

]
