from django.contrib import admin
from .models import Empleado, Habilidades

# Register your models here.
from .. import departamentos

admin.site.register(Habilidades)

#como ver la infromacion del modelo en el admin
class EmpleadoAdmin(admin.ModelAdmin):
    #organizacion de tabblas de tabblas
    # tablaba al final que no pertenece al modelo pero yo la hago
    list_display = ('first_name', 'last_name', 'departamentos',
                    'full_name','id')
    #funcion para la columna que vamos a agregar que no pertenece al modelo
    def full_name(self, obj):
        return obj.first_name + ' ' + obj.last_name
    #bucador por nombre
    search_fields = ('first_name',)
    # filtros para el admin. -organizar el admin
    list_filter = ('departamentos','job','habilidad',)
    #filtro de busqueda este filtro es de muchos a mucho - eledir la habilidad de la persona
    filter_horizontal = ('habilidad',)



admin.site.register(Empleado, EmpleadoAdmin)
